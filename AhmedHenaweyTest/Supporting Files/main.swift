//
//  main.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Henawey. All rights reserved.
//

import UIKit

private func delegateClassName() -> String? {
    let isTestTarget = NSClassFromString("XCTestCase") != nil
    return isTestTarget ? NSStringFromClass(TestAppDelegate.self) : NSStringFromClass(AppDelegate.self)
}

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, delegateClassName())
