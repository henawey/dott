//
//  LocationManager.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxCoreLocation
import CoreLocation
import RxSwift
import RxCocoa

protocol LocationManagerProtocol {
    func setup()
    var locationFilterSignal: Signal<CLLocation> { get }
}

struct LocationManager: LocationManagerProtocol {
    
    var locationFilterSignal: Signal<CLLocation> {
        return locationFilterRelay.asSignal()
    }
    
    fileprivate let locationFilterRelay = PublishRelay<CLLocation>()
    
    fileprivate let locationManager: CLLocationManager
    
    fileprivate let disposeBag = DisposeBag()
    init() {
        locationManager = CLLocationManager()
    }
    
    func setup() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.rx
            .didUpdateLocations
            .flatMap { Observable.from(optional: $0.locations.last) }
            .bind(to: locationFilterRelay)
            .disposed(by: disposeBag)
    }
}
