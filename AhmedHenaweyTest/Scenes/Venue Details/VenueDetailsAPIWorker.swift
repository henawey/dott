//
//  VenueDetailsAPIWorker.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/27/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxSwift

protocol VenueDetailsAPIWorkerLogic {
    func requestVenueDetails(venueId: String) -> Single<Details.Venue>
}

struct VenueDetailsAPIWorker: VenueDetailsAPIWorkerLogic {
    fileprivate let apiProvider: AnyAPIProvider<FoursquareEndpointProvider>
    
    init(apiProvider: AnyAPIProvider<FoursquareEndpointProvider>) {
        self.apiProvider = apiProvider
    }
    
    func requestVenueDetails(venueId: String) -> Single<Details.Venue> {
        return self.apiProvider
            .apiProvider
            .rx
            .request(.getDetailsOfAVenue(venueId))
            .map(expectedType: Details.Venue.self)
    }
}
