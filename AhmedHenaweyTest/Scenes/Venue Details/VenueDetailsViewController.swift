//
//  VenueDetailsViewController.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol VenueDetailsViewPersentLogic {
    var venueDetailsViewModel: VenueDetailsViewModelLogic? { get set }
    var resultRelay: PublishRelay<Details.Venue> { get }
}

class VenueDetailsViewController: UIViewController, VenueDetailsViewPersentLogic, VenueDetailsViewControllerHost {
    
    @IBOutlet weak fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var detailsLabels: [UILabel]!
    
    var venueDetailsViewModel: VenueDetailsViewModelLogic?
    var resultRelay = PublishRelay<Details.Venue>()
    var router: VenueDetailsRouterLogic!
    
    fileprivate var loadDataRelay = PublishRelay<SearchResult.Venue>()
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard venueDetailsViewModel != nil else {
            fatalError("Forget to set the view model for: \(String(describing: self))")
        }
        
        addCloseButton()
        setupBindings()
        loadDataRelay.accept(router.venue)
    }
    
    
    fileprivate func addCloseButton() {
        if #available(iOS 13.0, *), !isModalInPresentation {
            // don't do anything
        } else {
            // add button
            let closeButton = UIButton(type: .system)
            closeButton.setTitle("Cancel", for: .normal)
            closeButton.translatesAutoresizingMaskIntoConstraints = false
            
            view.addSubview(closeButton)
            
            NSLayoutConstraint.activate([
                closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                closeButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8)
            ])
            
            closeButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.router.close()
            }).disposed(by: disposeBag)   
        }
    }
    
    func setupBindings() {
        venueDetailsViewModel?
            .loadDataSignal = loadDataRelay.asSignal()
        
        venueDetailsViewModel?
            .loadingSignal
            .emit(onNext: { isLoading in
                if isLoading {
                    self.titleLabel.text = nil
                    self.detailsLabels.forEach { $0.text = nil }
                }
            }).disposed(by: disposeBag)
        
        venueDetailsViewModel?
            .errorSignal
            .emit(onNext: { [weak self] error in
                guard let self = self else { return }
                self.router.showError(title: "Error", message: "Something unexpected happen?") {
                    self.loadDataRelay.accept(self.router.venue)
                }
            }).disposed(by: disposeBag)
        
        venueDetailsViewModel?
            .resultSignal
            .emit(onNext: { venue in
                self.titleLabel.text = venue.name
                if let shortName  = venue.categories?.first?.shortName {
                    self.detailsLabels[0].text = "Category: \(shortName)"
                } else {
                    self.detailsLabels[0].text = "Category: Unknown"
                }
                
                if let tier  = venue.price?.tier {
                    let currecnySymbol: String
                    if Locale.current.currencyCode == "EUR" {
                        currecnySymbol = "€"
                    } else {
                        currecnySymbol = "$"
                    }
                    
                    var priceTier: String = ""
                    for _ in 0..<tier {
                        priceTier.append(currecnySymbol)
                    }
                    self.detailsLabels[1].text = "Price Tier: \(priceTier)"
                } else {
                    self.detailsLabels[1].text = "Currency: Unknown"
                }
                
                if let popular  = venue.popular?.richStatus?.text {
                    self.detailsLabels[2].text = "Popular: \(popular)"
                } else {
                    self.detailsLabels[2].text = "Popular: Unknown"
                }
                
                if let count = venue.beenHere?.count {
                    self.detailsLabels[3].text = "Been Here: \(count)"
                } else {
                    self.detailsLabels[3].text = "Been Here: Unknown"
                }
                
                if let count = venue.hereNow?.count {
                    self.detailsLabels[4].text = "Here Now: \(count)"
                } else {
                    self.detailsLabels[4].text = "Here Now: Unknown"
                }
                
                if let count = venue.likes?.count {
                    self.detailsLabels[5].text = "Likes: \(count)"
                } else {
                    self.detailsLabels[5].text = "Likes: Unknown"
                }
                
                if let rating = venue.rating {
                    self.detailsLabels[6].text = "Rating: \(rating)"
                } else {
                    self.detailsLabels[6].text = "Rating: Unknown"
                }
                
                if let isOpen = venue.hours?.isOpen {
                    self.detailsLabels[7].text = "Is Open: \(isOpen ? "Yes" : "No")"
                } else {
                    self.detailsLabels[7].text = "Is Open: Unknown"
                }
                
                if let richStatus = venue.hours?.richStatus?.text {
                    self.detailsLabels[8].text = "Status: \(richStatus)"
                } else {
                    self.detailsLabels[8].text = "Status: Unknown"
                }
                
                let dateFormatter = DateFormatter()
                if let createdAt = venue.createdAt {
                    let createdAtDate = Date(timeIntervalSince1970: TimeInterval(createdAt))
                    dateFormatter.dateStyle = .medium
                    dateFormatter.timeStyle = .none
                    let formattedDate = dateFormatter.string(from: createdAtDate)
                    self.detailsLabels[9].text = "Created At: \(formattedDate)"
                } else {
                    self.detailsLabels[9].text = "Created At: Unknown"
                }
                
            }).disposed(by: disposeBag)
    }
}
