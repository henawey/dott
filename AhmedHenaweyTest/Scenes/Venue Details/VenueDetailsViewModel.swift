//
//  VenueDetailsViewModel.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol VenueDetailsViewModelLogic {
    var resultSignal: Signal<Details.Venue> { get }
    var loadingSignal: Signal<Bool> { get }
    var errorSignal: Signal<VenuesError> { get }
    
    var loadDataSignal: Signal<SearchResult.Venue>? { get set }
}

class VenueDetailsViewModel: VenueDetailsViewModelLogic {
    
    var loadDataSignal: Signal<SearchResult.Venue>? {
        didSet {
            loadDataSignal?
                .map { $0.id }
                .emit(to: loadDataRelay)
                .disposed(by: disposeBag)
        }
    }
    
    // MARK: Output
    var resultSignal: Signal<Details.Venue> {
        return resultRelay.asSignal()
    }
    
    var loadingSignal: Signal<Bool> {
        return loadingRelay.asSignal()
    }
    
    var errorSignal: Signal<VenuesError> {
        return errorRelay.asSignal()
    }
    
    // MARK: Local Relaies
    fileprivate var resultRelay = PublishRelay<Details.Venue>()
    fileprivate var loadingRelay = PublishRelay<Bool>()
    fileprivate var errorRelay = PublishRelay<VenuesError>()
    fileprivate var loadDataRelay = PublishRelay<String>()
    
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate let apiWorker: VenueDetailsAPIWorkerLogic
    
    init(apiWorker: VenueDetailsAPIWorkerLogic) {
        self.apiWorker = apiWorker
        setupBindings()
    }
    
    func setupBindings() {
        loadDataRelay
            .do(onNext: { _ in self.loadingRelay.accept(true) })
            .subscribe(onNext: { [weak self] venueId in
                guard let self = self else { return }
                self.loadData(venueId: venueId)
            }).disposed(by: disposeBag)
    }
    
    // this is single, it will dispose any sequance merged or flat mapped with.
    fileprivate func loadData(venueId: String) {
        self.apiWorker.requestVenueDetails(venueId: venueId)
            .subscribe ({ [weak self] event in
                guard let self = self else { return }
                self.loadingRelay.accept(false)
                switch event {
                case .success(let result):
                    self.resultRelay.accept(result)
                case .error(let error):
                    let error = VenuesError.error(error)
                    self.errorRelay.accept(error)
                }
            }).disposed(by: disposeBag)
    }
}
