//
//  VenusDetailsRouter.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/27/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit

protocol VenueDetailsViewControllerHost: UIViewController {
    var router: VenueDetailsRouterLogic! { get }
}

protocol VenueDetailsRouterLogic: class {
    var host: VenueDetailsViewControllerHost! { get }
    var venue: SearchResult.Venue! { get }
    
    func loadScene(venue: SearchResult.Venue) -> VenueDetailsViewController
    func showError(title: String, message: String, retryAction: @escaping ()->()) 
    
    func close()
}

class VenueDetailsRouter: VenueDetailsRouterLogic {
    weak var host: VenueDetailsViewControllerHost!
    var venue: SearchResult.Venue!
    
    func loadScene(venue: SearchResult.Venue) -> VenueDetailsViewController {
        self.venue = venue
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: String(describing: VenueDetailsViewController.self)) as! VenueDetailsViewController
        let viewModel = DependancyFactroy.loadViewModel(for: VenueDetailsViewModelLogic.self)
        initialViewController.venueDetailsViewModel = viewModel
        initialViewController.router = self
        host = initialViewController
        return initialViewController
    }
    
    func showError(title: String, message: String, retryAction: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry?", style: .default, handler: { _ in
            retryAction()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        host.present(alert, animated: true, completion: nil)
    }
    
    func close() {
        host.dismiss(animated: true, completion: nil)
    }
}
