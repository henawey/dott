//
//  PreviewView.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import SwiftMessages

class PreviewView: MessageView {
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    @IBOutlet weak fileprivate var distanceLabel: UILabel!
    @IBOutlet weak fileprivate var addressLabel: UILabel!
    
    
    var showDetailsAction: ((_ id: String) -> Void)?
    var cancelAction: (() -> Void)?
    
    func configure(venue: SearchResult.Venue) {
        self.nameLabel.text = venue.name
        
        let formatter = MeasurementFormatter()
        formatter.locale = Locale.current
        self.distanceLabel.text = formatter.string(for: Measurement(value: Double(venue.location.distance), unit: UnitLength.meters))
        
        self.addressLabel.text = venue.location.formattedAddress.joined(separator: ", ")
    }
    
    
    @IBAction func goToDetailsTouchUpInside(_ sender: UIButton) {
        showDetailsAction?(id)
    }
    
    func show(id: String) {
        self.id = id
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.duration = .forever
        config.presentationStyle = .bottom 
        SwiftMessages.show(config: config, view: self)
    }
}
