//
//  VenuesViewController.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit
import MapKit
import SwiftMessages
import RxSwift
import RxCocoa

protocol VenuesViewPersentLogic {
    var venuesViewModel: VenuesViewModelLogic? { get set }
    var loadDataRelay: PublishRelay<Void> { get }
    var resultRelay: PublishRelay<[SearchResult.Venue]> { get }
}

class VenuesViewController: UIViewController, VenuesViewPersentLogic, VenuesViewControllerHost {
    
    @IBOutlet weak fileprivate var mapView: MKMapView!
    
    let loadDataRelay = PublishRelay<Void>()
    let resultRelay = PublishRelay<[SearchResult.Venue]>()
    
    fileprivate var visiableVenues: Set<SearchResult.Venue> = Set()
    fileprivate let annotationRelay = PublishRelay<[RestaurantAnnotation]>()
    fileprivate let mapVisiableRegionChangesRelay = PublishRelay<CLLocation>()
    fileprivate let disposeBag = DisposeBag()
    
    
    var venuesViewModel: VenuesViewModelLogic?
    var router: VenuesRouterLogic!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard venuesViewModel != nil else {
            fatalError("Forget to set the view model for: \(String(describing: self))")
        }
        
        setupMap()
        addTrackingButtonAndScale()
        setupBindings()
    }
    
    func setupBindings() {
        venuesViewModel?.mapVisiableRegionChangesSignal = mapVisiableRegionChangesRelay.asSignal()
        
        venuesViewModel?.loadDataSignal =
            loadDataRelay.asSignal()
        
        venuesViewModel?
            .resultSignal
            .emit(to: resultRelay)
            .disposed(by: disposeBag)
        
        resultRelay
            .do(onNext: { self.cacheVisiableVenuesSideEffect(venues: $0) })
            .map{ self.generateAnnotations(venues: $0) }
            .bind(to: annotationRelay)
            .disposed(by: disposeBag)
        
        venuesViewModel?
            .lazyLocationUpdatesSignal
            .emit(onNext: { location in
                if self.mapView.userTrackingMode != .none {
                    let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                              span: self.mapView.region.span)
                    
                    self.mapView.setRegion(coordinateRegion, animated: true)
                }
            }).disposed(by: disposeBag)
        
        venuesViewModel?
            .errorSignal
            .emit(onNext: { [weak self] error in
                guard let self = self else { return }
                self.router.showError(title: "Error", message: "Something unexpected happen?") {
                    self.loadDataRelay.accept(())
                }
            }).disposed(by: disposeBag)
        
        annotationRelay.subscribe(onNext: { annotations in
            // this just to make sure it's unique annotations
            let oldAnnotationSet = Set<RestaurantAnnotation>(self.mapView.annotations.compactMap { $0 as? RestaurantAnnotation })
            let newAnnotationsSet = Set<RestaurantAnnotation>(annotations)
            let differenceAnnotationsSet =
                newAnnotationsSet.subtracting(oldAnnotationSet)
            let unionAnnotationsArray = Array(differenceAnnotationsSet)
            
            self.mapView.addAnnotations(unionAnnotationsArray)
            if oldAnnotationSet.isEmpty { // only first time, we don't have lazy loading for the annotations. this should be acceptable for now.
                self.mapView.showAnnotations(unionAnnotationsArray, animated: true)
            }
        }).disposed(by: disposeBag)
    }
    
    func setupMap() {
        mapView.showsUserLocation = true
        
        mapView.centerCoordinate = mapView.userLocation.coordinate
        
        //New map type
        mapView.mapType = .mutedStandard
        mapView.delegate = self
        mapView.register(MarkerAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
    }
    
    func addTrackingButtonAndScale() {
        // add tracking button
        let trackingButton = MKUserTrackingButton(mapView: self.mapView)
        trackingButton.translatesAutoresizingMaskIntoConstraints = false
        trackingButton.layer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
        trackingButton.layer.borderColor = UIColor.white.cgColor
        trackingButton.layer.borderWidth = 1
        trackingButton.layer.cornerRadius = 5
        self.mapView.addSubview(trackingButton)
        
        let scale = MKScaleView(mapView: mapView)
        scale.legendAlignment = .trailing
        scale.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.addSubview(scale)
        
        NSLayoutConstraint.activate([
            trackingButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            trackingButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            scale.trailingAnchor.constraint(equalTo: trackingButton.leadingAnchor),
            scale.centerYAnchor.constraint(equalTo: trackingButton.centerYAnchor)
        ])
    }
    
    func cacheVisiableVenuesSideEffect(venues: [SearchResult.Venue]) {
        venues.forEach { venue in
            self.visiableVenues.insert(venue)
        }
    }
    
    func generateAnnotations(venues: [SearchResult.Venue]) -> [RestaurantAnnotation] {
        return venues.compactMap { RestaurantAnnotation(venue: $0) }
    }
}

extension VenuesViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if !animated {
            let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
            mapVisiableRegionChangesRelay.accept(location)
        }
    }
    
    func mapView(_ mapView: MKMapView, clusterAnnotationForMemberAnnotations memberAnnotations: [MKAnnotation]) -> MKClusterAnnotation {
        let clusterAnnotation = MKClusterAnnotation(memberAnnotations: memberAnnotations)
        clusterAnnotation.title = nil
        clusterAnnotation.subtitle = nil
        return clusterAnnotation
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let annotation = view.annotation as? RestaurantAnnotation else { return }
        SwiftMessages.hide(id: annotation.id)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard !(view.annotation is MKClusterAnnotation) else { return }
        guard let annotation = view.annotation as? RestaurantAnnotation else { return }
        guard let view: PreviewView = try? SwiftMessages.viewFromNib() else { return }
        guard let venue = self.visiableVenues.first(where: { venue -> Bool in return venue.id == annotation.id }) else { return }
        
        view.configure(venue: venue)
        view.showDetailsAction = { id in
            // show detailed screen
            self.router.showDetails(venue: venue)
            self.mapView.deselectAnnotation(annotation, animated: true)
            SwiftMessages.hide()
        }
        
        view.cancelAction = {
            self.mapView.deselectAnnotation(annotation, animated: true)
            SwiftMessages.hide()
        }
        view.show(id: annotation.id)
    }
}
