//
//  RestaurantAnnotation.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit
import MapKit

class RestaurantAnnotation: NSObject, MKAnnotation {
    
    let clusteringIdentifier = "Restaurant"
    
    let id: String
    let title: String?
    let color: UIColor = .red
    let coordinate: CLLocationCoordinate2D
    
    init(venue: SearchResult.Venue) {
        self.id = venue.id
        self.title = venue.name
        self.coordinate = CLLocationCoordinate2DMake(venue.location.lat, venue.location.lng)
    }
    
    override var hash: Int {
        return id.hashValue
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let new = object as? RestaurantAnnotation else {
            return false
        }
        
        return new.id == self.id
    }
}
