//
//  MarkerAnnotationView.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit
import MapKit

class MarkerAnnotationView: MKMarkerAnnotationView {

    override var annotation: MKAnnotation? {
        willSet {
            guard let annotation = newValue as? RestaurantAnnotation else { return }
            clusteringIdentifier = annotation.clusteringIdentifier
            markerTintColor = annotation.color
            glyphText = annotation.title
        }
    }
}

