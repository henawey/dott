//
//  VenuesRouter.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import UIKit

protocol VenuesViewControllerHost: UIViewController {
    var router: VenuesRouterLogic! { get }
}

protocol VenuesRouterLogic: class {
    var host: VenuesViewControllerHost! { get }
    func loadScene() -> VenuesViewController
    
    // navigation
    func showDetails(venue: SearchResult.Venue)
    func showError(title: String, message: String, retryAction: @escaping ()->())
}

class VenuesRouter: VenuesRouterLogic {
    weak var host: VenuesViewControllerHost!
    
    func loadScene() -> VenuesViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: String(describing: VenuesViewController.self)) as! VenuesViewController
        let viewModel = DependancyFactroy.loadViewModel(for: VenuesViewModelLogic.self)
        initialViewController.venuesViewModel = viewModel
        initialViewController.router = self
        
        host = initialViewController
        return initialViewController
    }
    
    func showError(title: String, message: String, retryAction: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry?", style: .default, handler: { _ in
            retryAction()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        host.present(alert, animated: true, completion: nil)
    }
    
    func showDetails(venue: SearchResult.Venue) {
        let scene = VenueDetailsRouter().loadScene(venue: venue)
        host.present(scene, animated: true, completion: nil)
    }
}
