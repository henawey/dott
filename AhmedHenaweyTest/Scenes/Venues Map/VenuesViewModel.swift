//
//  VenuesViewModel.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

public enum VenuesError {
    case locationUnknown
    case error(_ error: Swift.Error)
}

protocol VenuesViewModelLogic {
    // MARK: Output
    var resultSignal: Signal<[SearchResult.Venue]> { get }
    var lazyLocationUpdatesSignal: Signal<CLLocation> { get }
    var loadingSignal: Signal<Bool> { get }
    var errorSignal: Signal<VenuesError> { get }
    
    // MARK: Input
    var mapVisiableRegionChangesSignal: Signal<CLLocation>? { get set }
    var loadDataSignal: Signal<Void>? { get set }
}

class VenuesViewModel: VenuesViewModelLogic {
    // MARK: Input
    var loadDataSignal: Signal<Void>? {
        didSet {
            loadDataSignal?.emit(onNext: { [weak self] _ in
                guard let self = self else { return }
                
                guard let lastKnownLocation = self.locationWorker.lastKnownLocation else {
                    self.errorRelay.accept(.locationUnknown)
                    return
                }
                
                let request = VenuesAPIRequest(lat: lastKnownLocation.coordinate.latitude,
                                               long: lastKnownLocation.coordinate.longitude,
                                               radiusInMeters: Int(self.locationWorker.defaultRadius),
                                               categories: nil,
                                               query: "restaurant")
                self.apiRelay.accept(request)
            }).disposed(by: disposeBag)
        }
    }
    
    var mapVisiableRegionChangesSignal: Signal<CLLocation>? {
        didSet {
            mapVisiableRegionChangesSignal?
                .emit(to: locationWorker.locationFilterRelay)
                .disposed(by: disposeBag)
        }
    }
    
    // MARK: Output
    var resultSignal: Signal<[SearchResult.Venue]> {
        return resultRelay.asSignal()
    }
    
    var lazyLocationUpdatesSignal: Signal<CLLocation> {
        return locationWorker.currentLocationUpdatesSignal
    }
    
    var loadingSignal: Signal<Bool> {
        return loadingRelay.asSignal()
    }
    
    var errorSignal: Signal<VenuesError> {
        return errorRelay.asSignal()
    }
    
    // MARK: Local Relaies
    fileprivate var resultRelay = PublishRelay<[SearchResult.Venue]>()
    fileprivate var loadingRelay = PublishRelay<Bool>()
    fileprivate var errorRelay = PublishRelay<VenuesError>()
    fileprivate var apiRelay = PublishRelay<VenuesAPIRequest>()
    
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate let apiWorker: VenuesAPIWorkerLogic
    fileprivate let locationWorker: VenuesLocationWorkerLogic
    fileprivate var workerObservable: Observable<[SearchResult.Venue]>!
    
    init(apiWorker: VenuesAPIWorkerLogic,
         locationWorker: VenuesLocationWorkerLogic) {
        self.apiWorker = apiWorker
        self.locationWorker = locationWorker
        
        setupBindings()
    }
    
    func setupBindings() {
        
        locationWorker.currentLocationUpdatesSignal
            .map({ (location) -> VenuesAPIRequest in
                return VenuesAPIRequest(lat: location.coordinate.latitude,
                                        long: location.coordinate.longitude,
                                        radiusInMeters: Int(self.locationWorker.defaultRadius),
                                        categories: nil,
                                        query: "restaurant")
            })
            .emit(to: apiRelay)
            .disposed(by: disposeBag)
        
        apiRelay
            .subscribe(onNext: { [weak self] (request) in
                guard let self = self else { return }
                self.loadData(request: request)
            }).disposed(by: disposeBag)
            
    }
    
    // this is single, it will dispose any sequance merged or flat mapped with.
    fileprivate func loadData(request: VenuesAPIRequest) {
        self.apiWorker.requestVenues(request: request)
            .subscribe({ [weak self] event in
                guard let self = self else { return }
                self.loadingRelay.accept(false)
                switch event {
                case .success(let results):
                    self.resultRelay.accept(results)
                case .error(let error):
                    let error = VenuesError.error(error)
                    self.errorRelay.accept(error)
                }
            }).disposed(by: disposeBag)
    }
}

