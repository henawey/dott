//
//  VenuesWorker.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxSwift

protocol VenuesAPIWorkerLogic {
    func requestVenues(request: VenuesAPIRequest) -> Single<[SearchResult.Venue]>
}

struct VenuesAPIRequest {
    let lat: Double
    let long: Double
    let radiusInMeters: Int
    let categories: [CategoryTypes]?
    let query: String
}

struct VenuesAPIWorker: VenuesAPIWorkerLogic {
    fileprivate let apiProvider: AnyAPIProvider<FoursquareEndpointProvider>
    
    init(apiProvider: AnyAPIProvider<FoursquareEndpointProvider>) {
        self.apiProvider = apiProvider
    }
    func requestVenues(request: VenuesAPIRequest) -> Single<[SearchResult.Venue]> {
        return self.apiProvider
            .apiProvider
            .rx
            .request(.searchForVenues(lat: request.lat,
                                      long: request.long,
                                      radiusInMeters: request.radiusInMeters,
                                      categories: request.categories,
                                      query: request.query))
            .map(expectedType: [SearchResult.Venue].self)
    }
}
