//
//  VenuesLocationWorker.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation
import RxCoreLocation

protocol VenuesLocationWorkerLogic {
    // MARK: Input
    var locationFilterRelay: PublishRelay<CLLocation> { get }
    
    // MARK: Output
    var currentLocationUpdatesSignal: Signal<CLLocation> { get }
    var defaultRadius: Double { get }
    var lastKnownLocation: CLLocation? { get }
}

struct VenuesLocationWorker: VenuesLocationWorkerLogic {
    fileprivate let locationManager: CLLocationManager
    fileprivate let disposeBag = DisposeBag()
    
    let defaultRadius = 5000.0
    
    var lastKnownLocation: CLLocation? {
        return locationManager.location
    }
    
    let locationFilterRelay = PublishRelay<CLLocation>()
    var currentLocationUpdatesSignal: Signal<CLLocation> {
        return currentLocationUpdatesRelay.asSignal()
    }
    
    fileprivate let currentLocationUpdatesRelay = PublishRelay<CLLocation>()
    
    init() {
        locationManager = CLLocationManager()
        setupLocationServices()
    }
    
    func setupLocationServices() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        locationManager.rx
            .didUpdateLocations
            .flatMap { Observable.from(optional: $0.locations.last) }
            .bind(to: locationFilterRelay)
            .disposed(by: disposeBag)
            
        // filter all locations if it the distance between old and new locations less than 300 meters
        locationFilterRelay
            .distinctUntilChanged { $1.distance(from: $0) < self.defaultRadius / 2 }
            .bind(to: currentLocationUpdatesRelay)
            .disposed(by: disposeBag)
    }
     
}


