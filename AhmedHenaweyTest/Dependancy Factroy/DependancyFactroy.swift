//
//  DependancyFactroy.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

struct DependancyFactroy {
    static func loadViewModel<T>(for type: T.Type) -> T? {
        let factory = DependancyFactroy()
        if type == VenuesViewModelLogic.self {
           return factory.mapViewModel() as? T
        } else if type == VenueDetailsViewModelLogic.self {
            return factory.detailsViewModel() as? T
        } else {
            return nil
        }
    }
    
    fileprivate func mapViewModel() -> VenuesViewModelLogic {
        let locationWorker = VenuesLocationWorker()
        let endpointProvider = FoursquareEndpointProvider(environment: StaticEnvionment())
        let apiProvider = AnyAPIProvider(APIProvider(endpointProvider: endpointProvider))
        let apiWorker = VenuesAPIWorker(apiProvider: apiProvider)
        return VenuesViewModel(apiWorker: apiWorker,
                               locationWorker: locationWorker)
    }
    
    fileprivate func detailsViewModel() -> VenueDetailsViewModelLogic {
        let endpointProvider = FoursquareEndpointProvider(environment: StaticEnvionment())
        let apiProvider = AnyAPIProvider(APIProvider(endpointProvider: endpointProvider))
        let apiWorker = VenueDetailsAPIWorker(apiProvider: apiProvider)
        return VenueDetailsViewModel(apiWorker: apiWorker)
    }
}
