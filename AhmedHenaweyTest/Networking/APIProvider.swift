//
//  APIProvider.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Moya
import RxSwift

protocol APIProviderProtocol {
    associatedtype Endpoint: EndpointProvider
    var apiProvider: MoyaProvider<Endpoint.Spec> { get }
}

struct AnyAPIProvider<Endpoint: EndpointProvider>: APIProviderProtocol {
    internal var apiProvider: MoyaProvider<Endpoint.Spec>
    
    init<Provider: APIProviderProtocol>(_ provider: Provider) where Provider.Endpoint == Endpoint {
        apiProvider = provider.apiProvider
    }
}

class APIProvider<T: EndpointProvider>: APIProviderProtocol {
    typealias Endpoint = T
    
    fileprivate let plugins: [PluginType]
    
    init(endpointProvider: T) {
        plugins = endpointProvider.plugins
    }
    
    /// attach a plugin to show Network Activity Indicator on requrest and hide on receive response
    fileprivate lazy var networkActivityPlugin = NetworkActivityPlugin { (change,target) in
        DispatchQueue.main.async {
            switch change {
            case .began: UIApplication.shared.isNetworkActivityIndicatorVisible = true
            case .ended: UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    fileprivate lazy var networkLoggerPlugin = NetworkLoggerPlugin(verbose: false)
    
    lazy var apiProvider: MoyaProvider<T.Spec> = { [unowned self] in
        
        var plugins:[PluginType] = []
        
        plugins.append(networkActivityPlugin)
        plugins.append(networkLoggerPlugin)
        plugins.append(contentsOf: self.plugins)
        
        let moyaProvider = MoyaProvider<T.Spec>(plugins: plugins)
        
        return moyaProvider
        }()
}
