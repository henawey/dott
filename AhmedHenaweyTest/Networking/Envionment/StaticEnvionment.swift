//
//  StaticEnvionment.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

struct StaticEnvionment: Environment {
    var version: String {
       return "20190425"
    }
    
    var clientId: String {
        return "2ZXYYSO2OVECZL2IWDI3ISSGN4M4XCCYVJHWOMEZJ55VR12Q"
    }
    
    var clientSecret: String {
        return "5G4FXD4LQ3OP5M3A0CY44UCVQ0T4TLFBMVOFY2WYSTT1G1JP"
    }
}
