//
//  Environment.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

protocol Environment {
    var version: String { get }
    var clientId: String { get }
    var clientSecret: String { get }
}
