//
//  ClientCredentialsPlugin.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Moya

public protocol RequireClientCredentials {
    var requireClientCredentials: Bool { get }
}

public struct ClientCredentialsPlugin: PluginType {
    let environment: Environment
    
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        guard
            let authorizable = target as? RequireClientCredentials,
            authorizable.requireClientCredentials,
            let url = request.url else{
                return request
        }
        
        let cliendIdQueryItem = URLQueryItem(name: "client_id", value: environment.clientId)
        
        let cliendSecretQueryItem = URLQueryItem(name: "client_secret", value:  environment.clientSecret)
        
        var components = URLComponents(string: url.absoluteString)
        var quereyItems = components?.queryItems ?? [URLQueryItem]()
        quereyItems.append(cliendIdQueryItem)
        quereyItems.append(cliendSecretQueryItem)
        components?.queryItems = quereyItems
        
        var request = request
        request.url = components?.url
        return request
    }
}
