//
//  VersionPlugin.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Moya

public protocol RequireVersion {
    var requireVersion: Bool { get }
}

public struct VersionPlugin: PluginType {
    let environment: Environment
    
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        guard
            let authorizable = target as? RequireVersion,
            authorizable.requireVersion,
            let url = request.url else{
                return request
        }
        
        let urlqueryItem = URLQueryItem(name: "v", value: environment.version)
        
        var components = URLComponents(string: url.absoluteString)
        var quereyItems = components?.queryItems ?? [URLQueryItem]()
        quereyItems.append(urlqueryItem)
        components?.queryItems = quereyItems
        
        var request = request
        request.url = components?.url
        return request
    }
}
