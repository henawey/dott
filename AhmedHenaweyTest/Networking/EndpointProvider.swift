//
//  EndpointProvider.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import protocol Moya.PluginType
import protocol Moya.TargetType

protocol EndpointProvider {
    associatedtype Spec: TargetType
    
    var plugins: [PluginType] { get }
}
