//
//  FoursquareEndPointProvider.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import protocol Moya.PluginType
import protocol Moya.TargetType

struct FoursquareEndpointProvider: EndpointProvider {
    typealias Spec = FoursquareSpecification
    
    fileprivate var versionPlugin: VersionPlugin
    fileprivate var clientCredentialsPlugin: ClientCredentialsPlugin
    
    init(environment: Environment) {
        self.versionPlugin = VersionPlugin(environment: environment)
        self.clientCredentialsPlugin =  ClientCredentialsPlugin(environment: environment)
    }
    
    var plugins: [PluginType] {
        return [versionPlugin, clientCredentialsPlugin]
    }
}
