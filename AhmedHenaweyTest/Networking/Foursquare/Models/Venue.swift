//
//  Venue.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

enum SearchResult {
    // MARK: - Venue
    struct Venue: Codable, Equatable, Hashable {
        let id, name: String
        let location: Location
        let categories: [Category]
        let hasPerk: Bool
    }
    
    // MARK: - Category
    struct Category: Codable, Equatable, Hashable {
        let id: String
        let name: String
        let pluralName: String
        let shortName: String
        let icon: Categories.Icon
        let primary: Bool
    }
    
    // MARK: - Location
    struct Location: Codable, Equatable, Hashable {
        let address, crossStreet: String?
        let lat, lng: Double
        let distance: Int
        let postalCode: String?
        let cc: String
        let city, state: String?
        let country: String
        let formattedAddress: [String]
        let neighborhood: String?
    }
}
