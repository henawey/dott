//
//  Categories.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

enum Categories {
    // MARK: - Category
    struct Category: Codable {
        let id, name, pluralName, shortName: String
        let icon: Icon
        let categories: [Category]
    }
    
    // MARK: - Icon
    struct Icon: Codable, Equatable, Hashable {
        let prefix: String
        let suffix: String
    }
}
