//
//  VenueDetails.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

enum Details {
    // MARK: - Venue
    struct Venue: Codable {
        let id, name: String?
        let contact: Contact?
        let location: Location?
        let canonicalURL: String?
        let categories: [Category]?
        let verified: Bool?
        let stats: Stats?
        let url: String?
        let price: Price?
        let likes: HereNow?
        let dislike, ok: Bool?
        let rating: Double?
        let ratingColor: String?
        let ratingSignals: Int?
        let allowMenuURLEdit: Bool?
        let beenHere: BeenHere?
        let specials: Inbox?
        let photos: HereNow?
        let reasons: Inbox?
        let venueDescription: String?
        let hereNow: HereNow?
        let createdAt: Int?
        let tips: Listed?
        let shortURL: String?
        let timeZone: String?
        let listed: Listed?
        let hours, popular: Hours?
        let pageUpdates, inbox: Inbox?
        let parent: Parent?
        let hierarchy: [Hierarchy]?
        let attributes: Attributes?
        let bestPhoto: BestPhotoClass?
    }
    
    // MARK: - Location
    struct Location: Codable {
        let address, crossStreet: String?
        let lat, lng: Double?
        let postalCode, cc, neighborhood, city: String?
        let state, country: String?
        let formattedAddress: [String]?
    }
    
    // MARK: - Attributes
    struct Attributes: Codable {
        let groups: [AttributesGroup]?
    }
    
    // MARK: - AttributesGroup
    struct AttributesGroup: Codable {
        let type, name, summary: String?
        let count: Int?
        let items: [PurpleItem]?
    }
    
    // MARK: - PurpleItem
    struct PurpleItem: Codable {
        let displayName, displayValue: String?
        let priceTier: Int?
    }
    
    // MARK: - BeenHere
    struct BeenHere: Codable {
        let count, unconfirmedCount: Int?
        let marked: Bool?
        let lastCheckinExpiredAt: Int?
    }
    
    // MARK: - BestPhotoClass
    struct BestPhotoClass: Codable {
        let id: String?
        let createdAt: Int?
        let source: Source?
        let photoPrefix: String?
        let suffix: String?
        let width, height: Int?
        let visibility: String?
        let user: BestPhotoUser?
    }
    
    // MARK: - Source
    struct Source: Codable {
        let name: String?
        let url: String?
    }
    
    // MARK: - BestPhotoUser
    struct BestPhotoUser: Codable {
        let id, firstName, lastName, gender: String?
        let photo: IconClass?
    }
    
    // MARK: - IconClass
    struct IconClass: Codable {
        let photoPrefix: String?
        let suffix: String?
    }
    
    // MARK: - Category
    struct Category: Codable {
        let id, name, pluralName, shortName: String?
        let icon: IconClass?
        let primary: Bool?
    }
    
    // MARK: - Contact
    struct Contact: Codable {
        let facebook, facebookUsername, facebookName: String?
    }
    
    // MARK: - FluffyItem
    struct FluffyItem: Codable {
        let id, name, itemDescription, type: String?
        let user: ItemUser?
        let editable, itemPublic, collaborative: Bool?
        let url: String?
        let canonicalURL: String?
        let createdAt, updatedAt: Int?
        let photo: BestPhotoClass?
        let followers: Followers?
        let listItems: Inbox?
        let source: Source?
        let itemPrefix: String?
        let suffix: String?
        let width, height: Int?
        let visibility, text: String?
        let photourl: String?
        let lang: String?
        let likes: HereNow?
        let logView: Bool?
        let agreeCount, disagreeCount: Int?
        let lastVoteText: String?
        let lastUpvoteTimestamp: Int?
        let todo: Followers?
        let authorInteractionType, firstName, lastName, gender: String?
    }
    
    // MARK: - HereNowGroup
    struct HereNowGroup: Codable {
        let type: String?
        let count: Int?
        let items: [FluffyItem]?
        let name: String?
    }
    
    // MARK: - HereNow
    struct HereNow: Codable {
        let count: Int?
        let summary: String?
        let groups: [HereNowGroup]?
    }
    
    // MARK: - Followers
    struct Followers: Codable {
        let count: Int?
    }
    
    // MARK: - Inbox
    struct Inbox: Codable {
        let count: Int?
        let items: [InboxItem]?
    }
    
    // MARK: - InboxItem
    struct InboxItem: Codable {
        let id: String?
        let createdAt: Int?
        let summary, type, reasonName: String?
    }
    
    // MARK: - ItemUser
    struct ItemUser: Codable {
        let id, firstName, lastName, gender: String?
        let photo: PurplePhoto?
    }
    
    // MARK: - PurplePhoto
    struct PurplePhoto: Codable {
        let photoPrefix: String?
        let suffix: String?
        let photoDefault: Bool?
    }
    
    // MARK: - Hierarchy
    struct Hierarchy: Codable {
        let name, lang, id: String?
        let canonicalURL: String?
    }
    
    // MARK: - Hours
    struct Hours: Codable {
        let status: String?
        let richStatus: RichStatus?
        let isOpen, isLocalHoliday: Bool?
        let timeframes: [Timeframe]?
    }
    
    // MARK: - RichStatus
    struct RichStatus: Codable {
        let text: String?
    }
    
    // MARK: - Timeframe
    struct Timeframe: Codable {
        let days: String?
        let includesToday: Bool?
        let timeframeOpen: [Open]?
    }
    
    // MARK: - Open
    struct Open: Codable {
        let renderedTime: String?
    }
    
    // MARK: - Listed
    struct Listed: Codable {
        let count: Int?
        let groups: [HereNowGroup]?
    }
    
    // MARK: - Parent
    struct Parent: Codable {
        let id, name: String?
        let location: Location?
        let categories: [Category]?
    }
    
    // MARK: - Price
    struct Price: Codable {
        let tier: Int?
        let message, currency: String?
    }
    
    // MARK: - Stats
    struct Stats: Codable {
        let tipCount: Int?
    }
}
