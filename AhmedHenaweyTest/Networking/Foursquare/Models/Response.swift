//
//  Response.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Foundation

// MARK: - RawResponse
struct RawResponse<T: Codable>: Decodable {
    let meta: Meta
    let response: Response<T>
}

// MARK: - Meta
struct Meta: Decodable {
    let code: Int
    let requestId: String
}

// MARK: - Response
struct Response<T: Codable>: Decodable {
    let data: T
    
    enum CodingKeys: CodingKey {
        case categories
        case venues
        case venue
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        guard let firstKey = container.allKeys.first else {
            let context = DecodingError.Context(codingPath: container.codingPath, debugDescription: "empty Key")
            throw DecodingError.dataCorrupted(context)
        }
        
        self.data = try container.decode(T.self, forKey: firstKey)
    }
}
