//
//  Reactive+FoursquareAPI.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import RxSwift
import Moya

extension PrimitiveSequence where TraitType == SingleTrait, ElementType == Moya.Response{
    
    /// filter all successful status codes and handle Custom reponse
    ///
    /// - Returns: Observable
    func map<T: Codable>(expectedType: T.Type) -> Single<T> {
        return flatMap { (response) -> Single<T> in
            // try to get respone with successful status codes
            let response = try response.filterSuccessfulStatusCodes()
            
            let rawResponse = try response.map(RawResponse<T>.self)
            if 200 ... 299 ~= rawResponse.meta.code {
                return Single.just(rawResponse.response.data)
            } else {
                let newResponse = Moya.Response(
                    statusCode: rawResponse.meta.code,
                    data: response.data,
                    request: response.request,
                    response: response.response)
                throw MoyaError.statusCode(newResponse)
            }
        }
    }
}
