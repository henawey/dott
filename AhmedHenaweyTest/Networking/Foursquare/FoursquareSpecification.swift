//
//  FoursquareSpecification.swift
//  AhmedHenaweyTest
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import Moya

enum CategoryTypes: String {
    case food = "4d954b0ea243a5684a65b473"
}

enum FoursquareSpecification {
    case getVenueCategories
    case searchForVenues(lat: Double, long: Double, radiusInMeters: Int, categories: [CategoryTypes]?, query: String)
    case getDetailsOfAVenue(_ id: String)
}

extension FoursquareSpecification: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.foursquare.com/v2/venues/")!
    }
    
    var path: String {
        switch self {
        case .getVenueCategories:
            return "categories"
        case .searchForVenues:
            return "search"
        case .getDetailsOfAVenue(let id):
            return id
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .searchForVenues(let lat, let long, let radiusInMeters, let categories, let query):
            let keys = [
                "ll": "\(lat), \(long)",
                "radius": radiusInMeters,
                "categoryId": categories?.compactMap{ $0.rawValue }.joined(separator: ",") ?? "",
                "query": query
            ] as [String : Any]
            return .requestParameters(parameters: keys, encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}

extension FoursquareSpecification: RequireVersion {
    var requireVersion: Bool {
        return true
    }
}

extension FoursquareSpecification: RequireClientCredentials {
    var requireClientCredentials: Bool {
        return true
    }
}
