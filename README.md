# Dott Code Test
it's require nothing to prepare the app, as recommeded the Pods folder already in pushed to git repo.

## Dependency manager
[CocoaPods](https://cocoapods.org/)

## Architecture
 Following MVVM with RxSwift

 The Main Life Cycle:
 **MVVM**
 ![MVVM](BlockDiagram.jpg)
---
## Libraries
### [RxSwift](https://github.com/ReactiveX/RxSwift)
Rx is a generic abstraction of computation expressed through Observable<Element> interface.
This is a Swift version of Rx.

### [RxCocoa](https://github.com/ReactiveX/RxSwift/tree/master/RxCocoa)
RxCocoa is a reactive abstraction to manage UIKit.

### [RxCoreLocation](https://github.com/RxSwiftCommunity/RxCoreLocation)
RxCoreLocation is a reactive abstraction to manage Core Location.

### [Moya](https://github.com/Moya/Moya)
The basic idea of Moya is that we want some network abstraction layer that sufficiently encapsulates actually calling Alamofire directly. It should be simple enough that common things are easy, but comprehensive enough that complicated things are also easy.

### [SwiftMessages](https://github.com/SwiftKickMobile/SwiftMessages)
SwiftMessages is a very flexible view and view controller presentation library for iOS.

Message views and view controllers can be displayed at the top, bottom, or center of the screen, over or under the status bar, or behind navigation bars and tab bars. There are interactive dismiss gestures including a fun, physics-based one. Multiple background dimming modes. And a lot more!

## Worth to mention:
1. It's support dark mode for map venues scene and load more annotations while dragging and panning.
2. You can find postman collection with environment used in [Postman Collection and Environment](Postman Collection and Environment/)

## Work needed:
1. There is no smart loading for the annotations on map for long user session there is a high memory usage as it's keep all annotation on map.
2. More unit testing for other parts of the app.
3. Retrieve categories from Foursquare API to make sure all required categoies included, right now the API called without any categories only query "restaurant"

## Terminology
Scene: is screen with it connected componenets

## Time spent:
 2 days
