//
//  VenuesViewModelTests.swift
//  AhmedHenaweyTestTests
//
//  Created by Ahmed Henawey on 9/26/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import CoreLocation
import RxTest

@testable import AhmedHenaweyTest

class VenuesViewModelTests: XCTestCase {
    
    var testSchduler: TestScheduler!
    var disposeBag: DisposeBag!
    var mockDisplayLogic: MockDisplayLogic!
    var mockAPIWorkerLogic: MockAPIWorkerLogic!
    var mockLocationWorkerLogic: MockLocationWorkerLogic!
    
    override func setUp() {
        testSchduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        super.tearDown()
        testSchduler = nil
        disposeBag = nil
    }
    
    func testLoadOnCreated() {
        let location = SearchResult.Location(address: "MockAddress",
                                             crossStreet: "MockCrossStreet",
                                             lat: 0.0,
                                             lng: 0.0,
                                             distance: 1,
                                             postalCode: "MockPostalCode",
                                             cc: "MockCC",
                                             city: "MockCity",
                                             state: "MockState",
                                             country: "MockCountry",
                                             formattedAddress: ["MockFormattedAddress"],
                                             neighborhood: "MockNeighborhood")
        let category = SearchResult.Category(id: "MockCategoryID",
                                             name: "MockCategoryName",
                                             pluralName: "MockCategoryPluralName",
                                             shortName: "MockCategoryShortName",
                                             icon: Categories.Icon(prefix: "MockCategoryIconPrefix",
                                                                   suffix: "MockCategoryIconSuffix"),
                                             primary: true)
        let mockVenue = SearchResult.Venue(id: "MockID",
                                           name: "MockName",
                                           location: location,
                                           categories: [category],
                                           hasPerk: true)
        
        let mockLocation = CLLocation(latitude: 50.0, longitude: 50.0)
        
        let testObserver = testSchduler.createObserver([SearchResult.Venue].self)
        
        mockDisplayLogic = MockDisplayLogic()
        mockAPIWorkerLogic = MockAPIWorkerLogic(mockVenues: [mockVenue])
        mockLocationWorkerLogic = MockLocationWorkerLogic()
        
        // mimic acutal location
        let recoredLoaction = Recorded.next(0, mockLocation)
        self.testSchduler
            .createColdObservable([recoredLoaction])
            .bind(to: mockLocationWorkerLogic.currentLocationUpdatesRelay)
            .disposed(by: self.disposeBag)
        
        
        mockDisplayLogic.venuesViewModel = VenuesViewModel(
                   apiWorker: mockAPIWorkerLogic,
                   locationWorker: mockLocationWorkerLogic)
               
        mockDisplayLogic.setup()
        
        mockDisplayLogic
            .resultRelay
            .bind(to: testObserver)
            .disposed(by: self.disposeBag)
        
        testSchduler.start()
        
        let venue = Recorded.next(0, [mockVenue])
        XCTAssertEqual(testObserver.events, [venue])
    }
}

struct MockAPIWorkerLogic: VenuesAPIWorkerLogic {
    let mockVenuesRelay = PublishRelay<[SearchResult.Venue]>()
    
    let mockVenues: [SearchResult.Venue]
    init(mockVenues: [SearchResult.Venue]) {
        self.mockVenues = mockVenues
    }

    func requestVenues(request: VenuesAPIRequest) -> Single<[SearchResult.Venue]> {
        return Single.just(mockVenues)
    }
}

struct MockLocationWorkerLogic: VenuesLocationWorkerLogic {
    let defaultRadius: Double = 500
    var lastKnownLocation: CLLocation?
    
    var locationFilterRelay = PublishRelay<CLLocation>()
    var currentLocationUpdatesRelay = PublishRelay<CLLocation>()
    var currentLocationUpdatesSignal: Signal<CLLocation> {
        return currentLocationUpdatesRelay.asSignal()
    }
}

struct MockDisplayLogic: VenuesViewPersentLogic {
    
    var resultRelay = PublishRelay<[SearchResult.Venue]>()
    var loadDataRelay = PublishRelay<Void>()
    
    var venuesViewModel: VenuesViewModelLogic?
    
    func setup() {
        _ = venuesViewModel?
        .resultSignal
        .emit(to: resultRelay)
    }
}
