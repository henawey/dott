//
//  VersionPluginTests.swift
//  AhmedHenaweyTestTests
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import XCTest
import Moya
@testable import AhmedHenaweyTest

class VersionPluginTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidVersionNotAdded() {
        let targetType = MockTargetType()
        let urlRequest = URLRequest(url: targetType.baseURL)
        let versionPlugin = VersionPlugin(environment: MockEnvironment())
        let versionNotAddedRequest = versionPlugin.prepare(urlRequest, target: targetType)
        
        XCTAssertEqual(versionNotAddedRequest.url, urlRequest.url)
    }
    
    func testValidVersionAdded() {
        let environment = MockEnvironment()
        let targetType = MockRequireVersionTargetType()
        let urlRequest = URLRequest(url: targetType.baseURL)
        let versionPlugin = VersionPlugin(environment: environment)
        let versionAddedRequest = versionPlugin.prepare(urlRequest, target: targetType)
        
        XCTAssertNotEqual(versionAddedRequest.url, urlRequest.url)
        
        let hasVersionAsSuffiex = versionAddedRequest.url?.absoluteString.hasSuffix("v=\(environment.version)") ?? false
            
        XCTAssertTrue(hasVersionAsSuffiex, "Version not added at the end")
    }
    
    class MockTargetType: TargetType {
        var baseURL: URL {
            return URL(string: "http://www.MockServer.com/")!
        }
        
        var path: String {
            return "MockPath"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var sampleData: Data {
            return Data()
        }
        
        var task: Task {
            return .requestPlain
        }
        
        var headers: [String : String]? {
            return nil
        }
    }
    
    class MockRequireVersionTargetType: MockTargetType, RequireVersion {
        var requireVersion: Bool {
            return true
        }
    }
    
    struct MockEnvironment: Environment {
        var version: String {
            return "mockVersion"
        }
        
        var clientId: String {
            return "mockClientId"
        }
        
        var clientSecret: String {
            return "MockClientSecret"
        }
    }

}
