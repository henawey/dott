//
//  ClientCredentialsPluginTests.swift
//  AhmedHenaweyTestTests
//
//  Created by Ahmed Henawey on 9/25/19.
//  Copyright © 2019 Dott. All rights reserved.
//

import XCTest
import Moya
@testable import AhmedHenaweyTest

class ClientCredentialsPluginTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testValidClientIdAndSecretNotAdded() {
        let targetType = MockTargetType()
        let urlRequest = URLRequest(url: targetType.baseURL)
        let clientCredentialsPlugin = ClientCredentialsPlugin(environment: MockEnvironment())
        let clientCredentialsNotAddedRequest = clientCredentialsPlugin.prepare(urlRequest, target: targetType)
        
        XCTAssertEqual(clientCredentialsNotAddedRequest.url, urlRequest.url)
    }
    
    func testValidClientIdAndSecretAdded() {
        let environment = MockEnvironment()
        let targetType = MockRequireClientCredentialsTargetType()
        let urlRequest = URLRequest(url: targetType.baseURL)
        let clientCredentialsPlugin = ClientCredentialsPlugin(environment: environment)
        let clientCredentialsAddedRequest = clientCredentialsPlugin.prepare(urlRequest, target: targetType)
        
        XCTAssertNotEqual(clientCredentialsAddedRequest.url, urlRequest.url)
        
        guard let url = clientCredentialsAddedRequest.url else {
            XCTFail("Client Credentials generate empty url")
            return
        }
        
        let components = URLComponents(string: url.absoluteString)
        
        guard let queryItems = components?.queryItems else {
            XCTFail("Client Credentials generate empty query items")
            return
        }
        
        let expectedQuereyItemsArray = [
            URLQueryItem(name: "client_id", value: environment.clientId),
             URLQueryItem(name: "client_secret", value: environment.clientSecret)
        ]
        
        
        let actualQuereyItems = Set(arrayLiteral: queryItems)
        let expectedQuereyItems = Set(arrayLiteral: expectedQuereyItemsArray)
        
        XCTAssertEqual(actualQuereyItems, expectedQuereyItems, "Client Credentials have wrong values")
    }
    
    class MockTargetType: TargetType {
        var baseURL: URL {
            return URL(string: "http://www.MockServer.com/")!
        }
        
        var path: String {
            return "MockPath"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var sampleData: Data {
            return Data()
        }
        
        var task: Task {
            return .requestPlain
        }
        
        var headers: [String : String]? {
            return nil
        }
    }
    
    class MockRequireClientCredentialsTargetType: MockTargetType, RequireClientCredentials {
        var requireClientCredentials: Bool {
            return true
        }
    }
    
    struct MockEnvironment: Environment {
        var version: String {
            return "mockVersion"
        }
        
        var clientId: String {
            return "mockClientId"
        }
        
        var clientSecret: String {
            return "MockClientSecret"
        }
    }
    
}
